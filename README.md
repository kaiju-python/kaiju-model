## 1. Для чего нужна библиотека Kaiju Model?

Библиотека Kaiju Model предоставляет инструменты для управления моделями проекта и позволяет
стандартизировать данные, передаваемые на фронтенд.

_Модели_ требуются для валидации данных при создании или редактировании объекта клиентом, 
а также для корректного вывода данных (представления) объекта в интерфейсе.

> Библиотека Kaiju Model возвращает данные в формате, который использует фронтенд-библиотеки 
> **Kaiju.ui.componets & Kauju.ui.forms**.
Поэтому мы рекомендуем использовать эти библиотеки в связке для эффективной разработки бэкенда и фронтенда 
> вашего проекта.


## 2. Место моделей в структуре проекта

Мы предлагаем следующую структуру проекта (на примере создания сервиса "category"):

- директория приложения _app_
    - директория _modules_
        - директория _category_ 
          - файл **models.py** (содержит информацию о модели сервиса category)
          - файл **validators.py** (содержит валидаторы для модели category)
        - директория grid
          - файл **handlers.py** (содержит хэндлеры)
          - файл **__init__.py** (файл, регистрирующий хэндлеры)
    - директория _services_
        - файл **category.py** (содержит непосредественно код сервиса category)
    
Все элементы данной структуры будут рассмотрены ниже.

## 3. Пример простого сервиса

Как мы говорили выше, модель отвечает за получение и обработку данных объекта. 
Поэтому модель всегда используется в связке с сервисным кодом, 
который управляет созданием, редактированием и представлением объектов.

Продемонстрировать работу модели можно только на примере такого сервисного слоя.


### 3.1. Метод model (передача данных на фронтенд для отображения формы создания объекта)

Если вы хотите предоставить клиенту возможность создавать объекты на фронтенде, 
предлагаем создать статический метод **model**. 

В данном примере метод возвращает указанные в модели `CategoryCreateModel` поля, по которым будет "построена" страница.
Таким образом формируется форма для заполнения данных на основании данных, полученных из модели. 

    @staticmethod
    def model():
        return CategoryCreateModel.get_fields()

### 3.2. Метод create (создание объекта)

Метод model, который мы описали выше, отвечает за создание формы на фронтенде, но не за создание объекта. 

Для создания объекта мы предлагаем использовать метод **create**.

В данном примере метод принимает от клиента обязательный параметр `id` (параметры `labels` и `parent` опциональны).
Валидация осуществляется за счет передачи значения `id` в модель `CategoryCreateModel`, 
которая вызывается с помощью контекстного менеджера.

Если валидация проходит успешно, то данные передаются в конструктор объекта с помощью rpc-запроса. 
В ответе приходит результат создания объекта. 

    async def create(self, id, labels=None, parent=None): 
        async with CategoryCreateModel(self.app, init=True, id=id):
            result = await self.app.services.EntityClient.call(
                method="Category.create",
                params={
                    "id": id,
                    "labels": labels,
                    "parent": parent,
                }
            )
            return result

### 3.3. Метод get (получение данных объекта)

Чтобы получить данные об уже созданном объекте и корректно отобразить их, мы предлагаем создать метод **get**.

В данном примере метод get принимает аргумент `id` и передает его с помощью rpc-запроса. 
Данные, пришедшие в ответе, обрабатываются (валидируются, нормализируются) в модели `CategoryEditModel`,
которую вызывает конструктор `ModelFormConstructor`.

В результате мы получаем данные, подходящие для представления на фронтенде.

    async def get(self, id):
        result = await self.app.services.EntityClient.call(
            method="Category.get",
            params={
                "id": id
            }
        )
        model = CategoryEditModel(self.app, init=False, **result)

        async with ModelFormConstructor(self.app, model=model) as form:
            return form.form

TODO: отличие form.form and model.fields я поищу

### 3.4. Метод settings_model (передача данных на фронтенд для отображения формы редактирования объекта)

Мы используем модель `CreateModel` для создания метода **settings_model**, который будет возвращать клиенту 
информацию об объекте. 

Например, это может быть полезно для построения страницы редактирования объекта на фронтенде.


В этом примере мы получаем информацию об объекте по параметру id, извлекаем нужные данные 
(настройки под ключом `settings`) и затем инстанциируем с ними модель.
Затем на созданной модели выполняется метод `.fields`, который возвращает значения всех полей в модели. 

    async def settings_model(self, id, **_):
        export = await self.get(id)

        async with Export.CreateModel(self.app, init=True, **export["settings"]) as model:
            return model.fields


### 3.5. Метод update (редактирование данных объекта)

Если вы хотите дать пользователю возможность редактировать созданный объект, предлагаем использовать метод **update**.

В данном примере метод принимает данные, валидирует их с помощью модели **CategoryEditModel**, 
и затем передает их в другой метод.

Чем поможет валидация в данном примере? Например, валидатор может проверить, существует ли объект с данным `id`, а также
проверит корректность переданных на обновление данных 
(например, так в числовое поле `sort` не попадет ошибочно переданная строка).

    async def update(self, id, parent, sort):
        async with CategoryEditModel(self.app, init=True, id=id):
            return await self.app.services.EntityClient.call(
                method="Category.update",
                params={
                    "id": id,
                    "parent": parent,
                    "sort": sort,
                }
            )

Также использование модели может быть полезным в том случае, когда метод на обновление объекта может принимать 
нефиксированное число аргументов для обновления (в зависимости от свойств объекта).

Так, в примере ниже данные для обновления приходят в словаре именованных аргументов `kwargs`. Эти данные объединяются
с уже существующими (полученными в результате выполнения метода `get`) и затем на их базе инстанциируется модель 
**ExportEditModel**.


    async def update(self, id, **kwargs):
        data = await self.get(id=id)

        data = dict(data)
        data.update(**kwargs)

        async with ExportEditModel(self.app, **data) as model:
            await super().update(id=id, data=kwargs)

        return True


### 3.6. Метод grid (табличное отображение данных объектов)

Чтобы корректно отобразить данные нескольких объектов, используйте метод **grid**. 
Он позволяет применять различные фильтры к данным и использовать пагинацию.

В данном примере все полученные данные поступают в модель **CategoryGridModel**, которую инстанциирует метод
`GridConstructor`.

>Метод grid возвращает данные в том формате, который используют методы библиотек
**Kaiju.ui.componets & Kauju.ui.forms**.

    async def grid(self, locale, id=None, query=None, page=1, per_page=24, fields=fields):
        result = await self.app.services.EntityClient.call(
            method="Category.grid",
            params={
                "locale": locale,
                "page": page,
                "per_page": per_page,
                "query": query,
                "id": id,
            }
        )
        data = result.pop("data", [])
        _models = [CategoryGridModel(self.app, **i) for i in data]
        async with GridConstructor(self.app, models=_models, fields=fields, locale=locale) as gc:
            return {
                **result,
                "data": list(gc),
                "fields": fields
            }


## 4. Поля (fields) моделей

Ключевым элементом модели является **поле** (field). 
Поле описывает тип данных и может содержать некоторую логику (например, валидацию данных). 

Kaiju Model предлагает следующие поля для типов данных, которые используются в проекте:

* StringField (для строковых данных);
* TextField (для строковых данных с поддержкой RichText);
* BooleanField (для булевых значений);
* IntegerField (для целых числовых значений);
* DecimalField (для десятичных числовых значений);
* DateField (для данных о дате);
* DateTimeField (для данных о дате и времени);
* SelectField (для выбора одного значения из списка);
* MultiselectField (для выбора нескольких значений из списка);
* VideoField (для видео-файлов);
* PhotoField (для файлов изображений);
* JSONObjectField (для выбора JSON-объекта);
* DocumentField (для файлов других типов);


Также доступны специальные поля для решения типичных задач:

**SimpleIdField**

* Предназначено для строковых данных (наследуется от `StringField`), 
* Обязательно для заполнения (`required=True`), 
* Значение не может быть изменено после создания объекта (`read_only=True`)
* Валидируется регулярным выражением `r"^[a-z0-9_]+$"`,
* Нормализация: все буквенные символы переводятся в нижний регистр.


**UUIDField**

* Предназначено для строковых данных (наследуется от `StringField`), 
* Обязательно для заполнения (`required=True`),  
* Значение не может быть изменено после создания объекта (`read_only=True`)
* Валидируется регулярным выражением `r"\S+"`.

>Поля импортируются следующим образом:
> 
>    `from ..fields import SimpleIdField, TextField`

## 5. Параметры полей

Для каждого поля можно указать параметры, описывающие логику при обработке данных.

* **required** (True/False). Параметр определяет, должно ли быть заполнено это поле или его заполнение опционально. 
*  TODO:default (True/False) - ??? 
* TODO: disabled (True/False) - ???
* **is_system** (True/False). Параметр определяет, будет ли отображено данное поле на фронтенде 
  (например, при вызове метода `get_fields`).
* **group**. Параметр определяет, в какой группе полей будет отображено данное поле на фронтенде. 
  По-умолчанию = `"Group.general"` В таком случае поле будет отображено в группе с заголовком "Основные свойства".
* **read_only** (True/False). Параметр определяет, возможно ли редактирование данного поля
  или значение будет только отображаться (но будет недоступно для правки).
* **grid_handler**. Значением параметра указывается grid handler для поля (подробнее в соответствующем разделе).
* **options_handler**. Значением параметра указывается функция, отвечающая за выбор опции.
  Таким образом на фронтенде можно реализовать выпадающий список. 
  Как правило, в качестве options_handler'a мы используем функцию `load` для данного модуля 
  (функция load возвращает простой список доступных опций).
* **normalizer**. Значением параметра указывается функция, отвечающая за нормализацию 
  (преобразование в требуемый формат) вводимых данных.
  Например, в поле `SimpleIdField` по умолчанию используется нормалайзер `normalizer_value`,
  который преобразует все буквенные символы в строке в нижний регистр:
  

    def normalizer_value(value, **_):
        return str(value).lower()`

* TODO: validator - ???
* **field_validator**. Значением параметра указывается валидатор, который будет проверять валидность значения данного поля.
  (подробнее в разделе 6).
* TODO: dependence - ???
* **nested**. Значение параметра будет являться ключом в паре "ключ: значение" для данного поля. Например: 


    class SomeModel(BaseModel, abc.ABC):
       some_field = StringField(nested="settings")
       
     async with Model(some_field="foo") as model:
          data = model.to_dict()
          print(data)
    
    >>> `{"settings": "foo"}`

## 6. Валидаторы

Модели позволяют валидировать данные, которые поступают в поля в процессе инстанциирования модели. 
Для валидации поля используется параметр `field_validator`.

Согласно договоренности, валидаторы размещаются в файле **validators.py**, по адресу `app.modules._module_name_`,  
где module_name - имя сервиса, для объектов которого создается валидатор.

Если валидатор, который вы создаете, достаточно гибок для использования в разных модулях, рекомендуем
разместить его по адресу `app/modules/validators.py`.

Функции-валидаторы принимают следующие аргументы:
* app - объект приложения; 
* key - параметр (поле) валидации (например, id); 
* value - полученное значение поля; 
* ref - данные, на основе которых происходит создание объекта (опционально). 

Пример простого валидатора, который сравнивает полученное значение `value` 
с полученным значением `ref` и возбуждает ошибку `ValidationError` в случае, если value больше ref:

    from kaiju_tools.exceptions import ValidationError

    def f_max_value(key: str, value, ref, **__):
        if value > ref:
            raise ValidationError('Value is too big.', data=dict(key=key, value=value, code='ValidationError.ValueTooBig'))

## 7. GridHandler'ы

**GridHandler** является одним из параметров поля и отвечает за корректное отображение значения поля 
на фронтенде. 

То есть, GridHandler необходим в том случае, если вы планируете передавать в таблицу не дефолтное значение 
(`base_value`), а отформатированное 
(например, лэйбл в соответствии с языком локализации или дату и время в выбранном формате).

Библиотека Kaiju Model предоставляет класс **BaseHandler**, который является родителем для всех GridHandler'ов.

Мы предлагаем располагать GridHandler'ы по адресу: `app/modules/grid/handlers.py` .
Они должны быть зарегистрированы в файле `__init.py__`.

Пример хэндлера, наследующего классу BaseHandler:

    from kaiju_model.grid.base import BaseHandler
    
    
    class SimpleLabelHandler(BaseHandler):
    
        def call(self, values):
            _r = {}
            for _row in values:
                _r[_row["id"]] = _row["value"] or f"""[{_row["id"]}]"""
    
            return _r

Пример регистрации хэндлеров в файле `__init.py__`:

    from kaiju_model.grid.constructor import grid_handler_service
    
    from . import attributes
    from . import base
    from . import handlers
    
    grid_handler_service.register_classes_from_module(attributes)
    grid_handler_service.register_classes_from_module(base)
    grid_handler_service.register_classes_from_module(handlers)

## 8. Создание модели

Согласно договоренности, модель создается по адресу app.modules._module_name_ в файле `models.py`, 
где _module_name_ - имя сервиса, для которого создается модель.

Модель является классом. Мы предлагаем наследовать каждую модель от двух классов:

* абстрактного класса ABC, входящего в состав библиотеки abc Python (подробнее о нем можно прочитать в официальной документации: https://docs.python.org/3/library/abc.html#abc.ABC)
* класса BaseModel, входящего в состав Kaiju Model.

Пример объявления модели:

    import abc
    
    from kaiju_model.model import BaseModel
    from ..fields import SimpleIdField, LabelField

    class CategoryGridModel(BaseModel, abc.ABC):
        label = LabelField()
        id = SimpleIdField()


## 9. Типы моделей

Как мы говорили выше, модели служат для валидации и отображения данных. 
В проекте мы используем три основных типа моделей для разных целей:

* **CreateModel**. Модель, отвечающая за ввод и передачу данных при создании объекта; 

* **EditModel**. Модель, отвечающая за редактирование данных объекта;

* **GridModel**. Модель, отвечающая за представление данных объектов.


### 9.1. CreateModel (модель для создания объекта)

Модель CreateModel выполняет три функции:

1) Поля, указанные в ней, будут выведены на фронтенд при запросе клиента на создание объекта.
   Подробнее об этом - в разделе 3.1. 
    
2) Данные, введенные при создании объекта, будут провалидированы. Подробнее об этом - в разделе 3.2.

3) Модель возвращает данные объекта в ответ на запрос на его редактирование. Подробнее об этом - в разделе 3.4.


**Пример CreateModel**:

    from kaiju_model.model import BaseModel    

    class ExportCreateModel(BaseModel, abc.ABC):
        id = SimpleIdField(field_validator=export_exists_validator, required=True)
        label = StringField(required=False)
        periodic = BooleanField(required=True)
        kind = SelectField(required=True, options_handler="ExportKind.load")

В данной модели для модели Export указаны четыре поля. Обязательны для заполнения поля `id`, `periodic` и `kind`. 
Если они не будут заполнены, будет возбуждена ошибка и объект не будет создан. 
Поле `label` может быть не заполнено.

Значение поля `id` валидирует функция `export_exists_validator`, проверяющая, не существует ли объекта 
с таким же идентификатором.

Значением `kind` может быть только значение из списка, который приходит в ответе на запрос `ExportKind.load`. 
В противном случае будет возбуждена ошибка.
На фронтенде поле `kind` будет выглядеть как выбор из выпадающего списка. 

### 9.2. EditModel (модель для редактирования данных)

Модель EditModel выполняет две функции:

1) Поля, указанные в ней, будут выведены на фронтенд при запросе на просмотр данных объекта. 
Этот метод возвращает указанные в модели поля, по которым и будет "построена" страница. 
   Подробнее об этом - в разделе 3.3.

2) Валидация данных при редактировании объекта. Подробнее об этом - в разделе 3.5.


Пример EditModel:

    from kaiju_model.model import BaseModel 

    class ExportEditModel(BaseModel, abc.ABC):
        id = SimpleIdField(required=True)
        label = StringField(required=True)
        periodic = BooleanField(read_only=True)
        updated = DateTimeField(read_only=True)
        settings = JSONObjectField(read_only=True)
        info = JSONObjectField(read_only=True)

Поля `periodic`, `updated`, `settings` и `info` доступны только для чтения: изменения запрещены параметром 
`read_only=True`.

Значение поля `id` также нельзя изменить, так как поле SimpleIdField по умолчанию имеет параметр `read_only=True`.

В данном примере можно изменить только поле `label` - и оно не может быть пустым. 

В том случае, если не предполагается изменение данных объекта извне 
(и вы не создаете метод, отвечающий за редактирование объекта), то необходимость в EditModel отпадает. 

### 9.3. GridModel (модель вывода данных)

Модель GridModel служит для табличного вывода данных об объектах. Подробнее об этом читайте в разделе 3.6.

Пример GridModel:

    from kaiju_model.model import BaseModel 

    class ExportGridModel(BaseModel, abc.ABC):
        id = StringField()
        label = StringField()
        kind = SelectField(grid_handler=ExportKindGridHandler.__name__)
        periodic = BooleanField()
        status = StringField(grid_handler=ExportStatusGridHandler.__name__)
        updated = DateTimeField(grid_handler=DateTimeGridHandler.__name__)

У полей `kind`, `status`, `updated` указан grid_handler.

В том случае, если модуль не предполагает отображения данных в табличном виде 
(и вы не создаете метод grid и ему подобные), то необходимость в GridModel отпадает. 